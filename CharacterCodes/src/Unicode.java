import java.util.Scanner;

public class Unicode {

	 
	
	public static void main(String[] args) {
		
		
				
		Scanner sc= new Scanner(System.in);
		System.out.println("char for char->unicode or \n code for unicode->char or \n exit for exit");
		boolean loop = true;
		while (loop){
			String cmd = sc.next();
			switch(cmd){
			case "char":
				CharToInt();
				break;
			case "code":
				IntToChar();
				break;
			case "exit" :
				exitCmd();
				loop = false;
				break;
			}
		
		
	}

}

	

	private static void CharToInt() {

		Scanner sc = new Scanner(System.in);
		System.out.println("enter letter ");
		char a = sc.next().charAt(0);
		System.out.println("unicode for: " + a + " is: " + (int)a);
		
	}

	private static void IntToChar() {

		Scanner sc = new Scanner(System.in);
		System.out.println("number ");
		int a = sc.nextInt();
		System.out.println("unicode for: " + a + " is: " + (char)a);
		
	}

	private static void exitCmd() {
		System.out.println("Thank you for using Menu");
	}
	


		
	}
